<?php require('../main/message.php') ?>
<?php require('../main/function.php') ?>
<?php
if($_SESSION['loggedin'] && $_SESSION['loggedin']==true){
    
}else{
    redirect_to('index.php');
}
?>
<?php require('../main/db_connect.php') ?>
<?php
    require ('../main/template/header.php');
?>
    <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Forms
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Forms
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" action="create_admin.php" name="form" method="POST" enctype="multipart/form-data">

                            <div class="form-group">
                                <label>Admin Name</label>
                                <input class="form-control" name="admin_name">
                            </div>

                            <div class="form-group">
                                <label>Admin Email</label>
                                <input class="form-control" name="admin_email">
                            </div>

                            <div class="form-group">
                                <label>Admin Password</label>
                                <input class="form-control" name="admin_pass">
                            </div>
                            <button type="submit" class="btn btn-success" name="submit">Submit Button</button>
                           
                        </form>
                 </div>
            </div>

        </div>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
