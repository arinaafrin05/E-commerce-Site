<?php require('../main/message.php') ?>
<?php require('../main/db_connect.php') ?>
<?php require('../main/function.php') ?>
<?php

if (isset($_POST['submit'])) {

    move_uploaded_file($_FILES['product_image']['tmp_name'], "images/" . $_FILES['product_image']['name']);

    global $conn;

    $name = mysqli_real_escape_string($conn, $_POST['product_name']);
    $code = mysqli_real_escape_string($conn, $_POST['product_code']);
    $price = mysqli_real_escape_string($conn, $_POST['product_price']);
    $image = mysqli_real_escape_string($conn, $_FILES['product_image']['name']);
    $details = mysqli_real_escape_string($conn, $_POST['product_details']);
    $offer = mysqli_real_escape_string($conn, $_POST['product_offer']);
    $quantity = mysqli_real_escape_string($conn, $_POST['product_quantity']);
    $category = mysqli_real_escape_string($conn, $_POST['category_id']);
    $query = "INSERT INTO tbl_product(product_name,product_code,product_price,product_image,product_details,product_offer,product_quantity,category_id)VALUES('$name','$code','$price','$image','$details','$offer','$quantity','$category')";

//
//print_r($query);
//exit();
    $result = mysqli_query($conn, $query);

    if ($result) {
        $_SESSION["message"] = "Save Product Successfully";
        redirect_to('all_product.php');
    } else {
        die("query failed");
    }
}
?>

<?php

mysqli_close($conn);
?>