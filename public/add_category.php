<?php require('../main/message.php') ?>
<?php require('../main/function.php') ?>
<?php
if($_SESSION['loggedin'] && $_SESSION['loggedin']==true){
    
}else{
    redirect_to('index.php');
}
?>
<?php require('../main/db_connect.php') ?>
<?php 
    require('../main/template/header.php') 
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Category
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Add Category
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" action="create_category.php" enctype="multipart/form-data" method="POST">

                            <div class="form-group">
                                <label>Category Name</label>
                                <input class="form-control" name="category_name">
                            </div>

                            <div class="form-group">
                                <button name="submit" type="submit" class="btn btn-success">Submit Button</button>

                            </div>
                    </form>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

<?php require('../main/template/footer.php') ?>
