<?php require('../main/message.php') ?>
<?php require('../main/function.php') ?>
<?php
if($_SESSION['loggedin'] && $_SESSION['loggedin']==true){
    
}else{
    redirect_to('index.php');
}
?>
<?php require('../main/db_connect.php') ?>
<?php 
    require('../main/template/header.php') 
?>

<?php
    $conn= mysqli_connect("localhost", "root", "", "db_ecommerces");
    if(!$conn){
        die("connection faild:").mysqli_connect_error();
    }
    
    $query = "SELECT * FROM tbl_admin";

    $result = mysqli_query($conn, $query);
 
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            All Admin
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="dashboard.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> All Admin
                                <?php
                                    echo message();
                                ?>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <h2>Bordered Table</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>EMAIL</th>
                                        <th>PASSWORD</th>
                                        <th>EDIT/DELETE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        ?>                                 
                                <tr>
                                    <td><?php echo $row['admin_id']; ?></td>
                                    <td><?php echo $row['admin_name']; ?></td>
                                    <td><?php echo $row['admin_email']; ?></td>
                                    <td><?php echo $row['admin_pass']; ?></td>
                                    <td>
                                        <a href="edit_admin.php?admin_id=<?php echo $row['admin_id']; ?>" class="btn btn-success">Edit</a>
                                        <a href="delete_admin.php?admin_id=<?php echo $row['admin_id']; ?>" class="btn btn-danger">DELETE</a>
                                    </td>
                                </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
