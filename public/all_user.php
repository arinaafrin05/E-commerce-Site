<?php require('../main/message.php') ?>
<?php require('../main/function.php') ?>
<?php
if($_SESSION['loggedin'] && $_SESSION['loggedin']==true){
    
}else{
    redirect_to('index.php');
}
?>
<?php require('../main/db_connect.php') ?>
<?php 
    require('../main/template/header.php') 
?>

<?php
    $query = "SELECT * FROM tbl_user";

    $result = mysqli_query($conn, $query);
    confirm($result);
 
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            All User
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> All User
                                <?php 
                                    echo message();
                                ?>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <h2>Bordered Table</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>PHONE NUMBER</th>
                                        <th>EMAIL</th>
                                        <th>PASSWORD</th>
                                        <th>DELETE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        ?>                                 
                                <tr>
                                    <td><?php echo $row['user_id']; ?></td>
                                    <td><?php echo $row['user_name']; ?></td>
                                    <td><?php echo $row['user_phone']; ?></td>
                                    <td><?php echo $row['user_email']; ?></td>
                                    <td><?php echo $row['user_pass']; ?></td>
                                    <td>
                                        <a href="delete_user.php?user_id=<?php echo $row['user_id']; ?>" class="btn btn-danger">DELETE</a>
                                    </td>
                                </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
<?php
mysqli_free_result($result);
?>
<?php require('../main/template/footer.php') ?>
