<?php require('../main/message.php') ?>
<?php require('../main/function.php') ?>
<?php
if($_SESSION['loggedin'] && $_SESSION['loggedin']==true){
    
}else{
    redirect_to('index.php');
}
?>
<?php
    require ('../main/db_connect.php');
?><?php
    require ('../main/template/header.php');
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Statistics Overview</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                                <?php
                                    echo message();
                                    echo $_SESSION["name"];
                                ?>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-right">
                                        <div class="huge">
                                            <h3>Products</h3>
                                        </div>
                                        <div>
                                            <?php 
                                                $sql = "SELECT * FROM tbl_product";
                                                $result = mysqli_query($conn,$sql);
                                                $count= mysqli_num_rows($result);
                                                echo  $count;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="all_product.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-right">
                                        <div class="huge">
                                            <h3>Users</h3>
                                        </div>
                                        <div>
                                            <?php 
                                                $sql = "SELECT * FROM tbl_user";
                                                $result = mysqli_query($conn,$sql);
                                                $count= mysqli_num_rows($result);
                                                echo  $count;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="all_user.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-right">
                                        <div class="huge">
                                            <h3>Admin</h3>
                                        </div>
                                        <div>
                                            <?php 
                                                $sql = "SELECT * FROM tbl_admin";
                                                $result = mysqli_query($conn,$sql);
                                                $count= mysqli_num_rows($result);
                                                echo  $count;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="all_admin.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Area Chart</h3>
                            </div>
                            <div class="panel-body">
                                <div id="morris-area-chart"></div>
                            </div>
                        </div>
                    </div>
                </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
