<!DOCTYPE HTML>
<html>
	<head>
	
		<title>Privacy Policy</title>
		<link rel="stylesheet" type="text/css" href="font_end/privacy.css">
		 <link href="font_end/css/font-awesome.min.css" rel="stylesheet"/>
		
	</head>
	
	<body>
		<div class="full-part">
			<div class="main-part">
				<div class="first-part">
					<a class="purple" href=""> VIEW ALL LEGAL AGREEMENTS</a>
				</div>
				<div class="second-part">
					<div class="text-one">
						<h2 class="text"> SHOPHIA WEB SITE USER PRIVACY NOTICE</h2>
					</div>
				</div>
				<div class="third-part">
					<div class="text-two">
						<h4 class="text"> EFFECTIVE DATE</h4>
						<p class="pragraph"> This notice was updated on February, 2016</p>
					</div>
				</div>
				<div class="fourth-part">
					<div class="text-three">
						<h3 class="text"> YOUR PRIVACY RIGHTS</h3>
						<p class="pragraph"> X.commerce, Inc. dba Shophia, Inc. (�Shophia�) has been awarded TRUSTe's Privacy Seal signifying that this Privacy Notice and practices have been reviewed by TRUSTe for compliance with TRUSTe's program requirements including transparency, accountability and choice regarding the collection and use of personal information. The TRUSTe program covers only information that is collected through the Shophia website, www.Shophia.com, www.imagineecommerce.com and www.Shophiacommerce.com, and other Shophia sites. The TRUSTe programs do not apply to information that may be collected through mobile devices.</p>
						
						<p class="pragraph">If we cannot answer your privacy-related questions, please use the TRUSTe Watchdog Dispute Resolution Process.</p>
					</div>
				</div>
				<div class="table-content-part">
					<div class="text-four">
						
					</div>
					<div class="table-content-part-one">
						<div class="icon-one">
							<img src="font_end/img/pic1.png" />
						</div>
						<div class="icont-description-one">
							<h3 class="pic-0ne">SCOPE AND CONSENT</h3>
							<p class="pragraph">This privacy notice describes your privacy rights regarding our collection, use, disclosure, retention, and protection of your personal information. It applies to any Shophia site where this notice appears in the footer, and to any Shophia application, service, or tool where this notice is referenced, regardless of how you access or use them, including mobile devices. You should read it to learn how Shophia will process and protect your data.</p>

							<p class="pragraph">By using this site or any related sites where this Privacy Notice is displayed or referenced, you expressly consent to the terms of this Privacy Notice.</p>

							<p class="pragraph">If all or part of Shophia is sold, merged, or otherwise transferred to another entity, the information gathered by the transferred entity may be transferred as part of that transaction.<p>

							<p class="pragraph">This Privacy Notice does not apply to the data we process on behalf of our clients. If your personal information was provided to us by a client and you wish to update, correct or delete it, please contact that company directly.</p>
							
							<h4 >PERSONAL INFORMATION</h4>
							<p class="pragraph">Throughout this Privacy Notice, we use the term �personal information� to describe information that identifies a specific person, such as full name, email address or mailing address. We do not consider personal information to include: business contact information, business information publicly available on the internet or other public sources, information that has been made anonymous or aggregated so that it can no longer be used, whether in combination with other information or otherwise, to identify a specific person, or information that merely identifies a computer or browser, or an ID number assigned to an anonymous person.</p>
							
							<h4>CHANGES TO THIS NOTICE</h4>
							
							<p class="pragraph">We may amend this Privacy Notice at any time by posting the amended terms on this Site. Any such amendment shall take effect when such amendment is sent by email or posted on the Site. If we make any material amendments we will notify you by email (sent to the email address you provided) or by means of a notice on this Site prior to the amendment taking effect.</p>
						</div>
						</div>
						<hr>
					<div class="table-content-part-one">
						<div class="icon-two">
							<img src="font_end/img/pic2.png" />
						</div>
						<div class="icont-description-one">
							<h3 class="pic-0ne">GLOBAL PRIVACY STANDARDS</h3>
							<h4>SAFE HARBOR CERTIFICATION</h4>
							<p class="pragraph">Shophia Sites comply with the US-EU Safe Harbor Framework and the US-Swiss Safe Harbor Framework as set forth by the US Department of Commerce regarding the collection, use, and retention of personal information from European Union member countries and Switzerland.</p>

							<p class="pragraph">Shofia has certified that its sites adhere to the Safe Harbor Privacy Principles of notice, choice, onward transfer, security, data integrity, access, and enforcement. To learn more about the Safe Harbor program, and to view our certification, please visit http://www.export.gov/safeharbor/.</p>

							<p class="pragraph">If you feel that this company is not abiding by its posted Privacy Notice, you should first contact Shophia by email at privacy@Shophia.com. As part of our participation in the Safe Harbor, we have agreed to TRUSTe dispute resolution for disputes relating to our compliance with the Safe Harbor Privacy Framework. If you have any complaints regarding our compliance with the Safe Harbor you should first contact us (as provided above). If contacting us does not resolve your complaint, you may raise your complaint with TRUSTe by Internet here, fax to 415-520-3420, or mail to TRUSTe Safe Harbor Compliance Dept., click for mailing address. If you are faxing or mailing TRUSTe to lodge a complaint, you must include the following information: the name of company, the alleged privacy violation, your contact information, and whether you would like the particulars of your complaint shared with the company. For information about TRUSTe or the operation of TRUSTe's dispute resolution process, click here or request this information from TRUSTe at any of the addresses listed above. If you do not receive acknowledgment of your inquiry or your inquiry has not been satisfactorily addressed, you should then contact the TRUSTe or Direct Marketing Association Safe Harbor Dispute Resolution Programs. They will then serve as a liaison to Shophia to resolve your concerns.</p>
						</div>
					</div>
					<hr>
					<div class="table-content-part-one">
						<div class="icon-three">
							<img src="font_end/img/pic3.png" />
						</div>
						<div class="icont-description-two">
							<h3 class="pic-0ne">SECURITY</h3>
							
							<p class="pragraph">We store and process your personal information on our computers in the US and elsewhere in the world where our facilities are located. We protect your information using reasonable technical and administrative security measures in an effort to reduce the risks of loss, misuse, unauthorized access, disclosure and alteration. Some of the safeguards we use are firewalls and data encryption, physical access controls to our data centers, and information access authorization controls. You acknowledge and agree that our security measures are not a guarantee of security and that we are not responsible for loss or misuse of your personal information.</p>
						</div>
					</div>
					<hr>
					<div class="table-content-part-one">
						<div class="icon-four">
							<img src="font_end/img/pic4.png" />
						</div>
						<div class="icont-description-two">
							<h3 class="pic-0ne">IMPORTANT INFORMATION</h3>
							<h4>CHILDREN�S PRIVACY</h4>
							<p class="pragraph">The SHOPHIA Sites are general audience websites and are not intended for children under the age of 13. We do not knowingly collect personal information via our Sites from users in this age group. If we discover we have inadvertently gathered any personal information from this age group, appropriate steps will be taken to delete it.</p>
							
							<h4>THIRD PARTIES</h4>
							<p class="pragraph">This Privacy statement only applies to information received or collected by SHOPHIA Sites. Our Sites do not control the privacy practices of third parties. Consumers who click on a link or advertisement, visit a third-party site, or interact with third-party content are subject to the privacy policies of those third parties, where applicable.</p>
						</div>
					</div>
					<hr>
					
					<div class="table-content-part-one">
						<div class="icon-six">
							<img src="font_end/img/pic6.png" />
						</div>
						<div class="icont-description-two">
							<h3 class="pic-0ne">COLLECTION</h3>
							<h4>WHY WE COLLECT INFORMATION</h4>
							<p class="pragraph">We and our affiliates may collect and use the information collected at our Sites for any internal purpose and provide that information to our business partners and vendors to be used by them related such purposes. For example, we may use this data to monitor our Sites and collect aggregate data for marketing purposes and for other future uses, or to determine the most appropriate information to send to those who request it.</p>

							<p class="pragraph">You may choose voluntarily to give us personally identifiable information, such as your name, address, phone number, email address, or other information about yourself. In addition, we may collect non-personal information automatically as you navigate our sites. This information may include (but may not be limited to): location data, IP address, device data such as mobile identifiers, carrier network information, the mobile website or mobile app used, and cookie or pixel tag data.</p>

							<p class="pragraph">Certain forms on our sites may also request contact and profile information, such as user name, email address, website, phone number, or company information. Completion of these forms is entirely voluntary but may be required in order to request certain services from SHOPHIA. R�sum� submissions on our Sites are used to screen candidates and contact them for interviews.

							<p class="pragraph">SHOPHIA may also use our sites to periodically survey our Clients for purposes of improving the quality of our service and customer satisfaction. Client participation is completely voluntary, and any information we collect from you as part of our surveys will be treated with the same respect as any other information gathered in accordance with this Notice.</p>
							
							<h4>HOW WE COLLECT IT</h4>
							<p class="pragraph">Our Sites use web beacons and other technologies to collect information when you access our websites, and we may store that information in �cookies�.</p>
							
							<p class="pragraph">We only obtain personally identifying information if it is supplied voluntarily. If you have chosen to receive marketing messages from us about our products and services, we may use information from our log files, cookies, or web beacons to help us make those communications more useful or interesting to you. We may periodically track the open-rates and click behavior of our emails and other messages and advertisements to help us identify which portions of our advertisements are most popular and effective with our customers.</p>
							
							<h4>HOW WE USE COOKIES AND OTHER TECHNOLOGIES</h4>
							<p class="pragraph">When you access our sites, we and our affiliates, business partners and vendors may place small data files on your computer or other device. We use these data files, commonly known as �Cookies� to help us identify you as a site visitor or Client, customize our content and enhance your site experience. These third parties may also use a technology called �Tags� or �Web Beacons�. Similar in function to Cookies, Web Beacons allow us to understand the movements of visitors to our Sites. The tracking technologies on our Sites do not collect personal information. We use this information to analyze trends, to administer our Sites, to track users' movements around the Sites, and to gather demographic information about our user base as a whole. SHOPHIA has no access to or control over third party cookies or tracking technologies, but does not provide personal information through the use of web beacons and cookies to these parties.</p>
							
							<p class="pragraph">You should note that although most browsers are initially set up to accept cookies, you may be able to change your browser settings to cause your browser to refuse first party or third party cookies or to indicate when a third party cookie is being sent. However, disabling or limiting cookies may cause certain features of this site to not function properly. Check your browser�s �Help� files or other similar resources to learn more about handling cookies on your browser.</p>
							<h4>DO NOT TRACK</h4>
							<p class="pragraph">California law requires us to state how we respond to Do Not Track (DNT) signals. Because there is currently no industry or legal standard for recognizing or honoring DNT, we do not currently respond to DNT signals. We await the result of work by the privacy community and industry to determine when such a response is appropriate and what form it should take.</p>
						</div>
					</div>
					<hr>
					<div class="table-content-part-one">
						<div class="icon-seven">
							<img src="font_end/img/pic7.png" />
						</div>
						<div class="icont-description-two">
							<h3 class="pic-0ne">CHOICE</h3>
							
							<p class="pragraph">You have a choice about how we use your personal information to communicate with you, send you marketing information, and provide you with customized and relevant advertising.</p>
							<h4>MARKETING</h4>
							<p class="pragraph">We do not sell or rent your personal information to third parties for their marketing purposes without your explicit consent. We may combine your information with information we collect from other companies and use it to improve and personalize our services or content.</p>
							<h4>OPT OUT</h4>
							<p class="pragraph">You may opt-out of the marketing messages we send at any time by utilizing the unsubscribe information provided in such message or by contacting us at privacy@SHOPHIA.com</p>
							
							<p class="pragraph">If you choose to unsubscribe from emails, please click unsubscribe in the applicable email message. Rarely, we may need to contact you regarding non-promotional purposes. You cannot opt out of these operational emails.</p>
						</div>
					</div>
					<hr>
					<div class="table-content-part-one">
						<div class="icon-eight">
							<img src="font_end/img/pic8.png" />
						</div>
						<div class="icont-description-two">
							<h3 class="pic-0ne">ACCESS TO INFORMATION</h3>
						
							<p class="pragraph">If you have provided us with your personal information and wish to update, correct, or delete it, you may have the ability to do so by contacting us as indicated below. We will attempt to respond to any access requests within 30 days.</p>

							<p class="pragraph">If your personal information was provided to us by a client and you wish to update, correct or delete it, please contact that company directly.</p>
						</div>
					</div>
					<hr>
					<div class="table-content-part-one">
						<div class="icon-five">
							<img src="font_end/img/pic5.png" />
						</div>
						<div class="icont-description-two">
							<h3 class="pic-0ne">QUESTIONS OR COMPLAINTS</h3>
							<p class="pragraph">Questions regarding this Privacy Notice should be directed to SHOPHIA by email to privacy@SHOPHIA.com or by regular mail to the attention of:</p>

							<p class="pragraph">Legal/Privacy Department<br>SHOPHIA, Inc.<br>10441 Jefferson Blvd., Suite 200<br>Culver City, CA 90232</p>
						
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</body>
</html>