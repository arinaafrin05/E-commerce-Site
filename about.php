<html>
	<head>
		<title>About us</title>
		<link rel="stylesheet" type="text/css" href="font_end/about.css"/>
	</head>
	<body>
		<div class="first">
			<img src="font_end/img/about.jpg" alt="font_end/img/about-us.jpg">
			<div class="text">
				<h1>ABOUT SOPHIA</h1>
				<p>Sophia Commerce is the leading platform for open commerce innovation
				with over $50B in gross merchandise volume transacted on the platform annually.</p>
			</div>	
		</div>
		<div class="second">
			<div class="text1">
				<p>Recognized as the leading eCommerce platform to the 2015 Internet Retailer Top 1000, 
				B2B 300 and Hot 100 lists, Sophia Commerce works hand in hand with retailers,
				brands and branded manufacturers across B2C and B2B industries to successfully integrate digital and physical shopping experiences.
				In addition to its flagship open source eCommerce platform, Sophia Commerce boasts a strong portfolio of cloud-based omnichannel
				solutions including in-store, retail associate and order management technologies. Sophia Commerce is supported by a vast global 
				network of 300+ solution and technology partners and by a highly active global community of more than 66,000 developers as well as 
				the largest eCommerce marketplace for extensions available for download on the Sophia Marketplace.</p>
			</div>
		</div>
		<div class="third">
			<div class="left">
				<div class="permira">
					<p><b><h2>PERMIRA</h2></b></p>
				</div>
			</div>
			<div class="right">
				<div class="sophia">
					<h1>SOPHIA IS NOW BACKED BY PERMIRA</h1>
					<p>Sophia Commerce is now backed by the Permira funds. Founded in 1985, the firm advises funds with a total committed capital
					of approximately �25 billion. Permira is a global investment firm that finds and backs successful businesses with ambition. 
					The Permira funds make long-term investments in companies with the objective of transforming their performance and driving 
					sustainable growth. In the past 30 years, the Permira funds have made over 200 private equity investments in five key sectors: 
					Consumer, Financial Services, Healthcare, Industrials, and Technology. Permira employs over 200 people including 120 investment
					professionals in 14 offices across North America, Europe, the Middle East, and Asia.</p>
					<input type="submit" name="LEARN MORE ABOUT PERMIRA" value="LEARN MORE ABOUT PERMIRA"/>
				</div>
			</div>
		</div>
	</body>
</html>