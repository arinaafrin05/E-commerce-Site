<?php require('main/message.php') ?>
<?php require('main/function.php') ?>
<?php require('main/db_connect.php') ?>
<?php
    if(isset($_POST["submit"])){
        $inquery= mysqli_real_escape_string($conn,$_POST['mail_inquery']);
        $f_name= mysqli_real_escape_string($conn,$_POST['mail_f_name']);
        $l_name= mysqli_real_escape_string($conn,$_POST['mail_l_name']);
        $email= mysqli_real_escape_string($conn,$_POST['mail_email']);
        $sql="INSERT INTO tbl_mail (mail_inquery,mail_f_name,mail_l_name,mail_email)VALUES('$inquery','$f_name','$l_name','$email')";
        
        $result= mysqli_query($conn, $sql);
        if(!$result){
            die("query faild");    
        }else{
            $_SESSION["message"] = "Save Successfully";
            redirect_to('contact.php');
        }
        
    }
?>
<html>
	<head>
		<title>contact us</title>
		<link rel="stylesheet" type="text/css" href="font_end/contact.css"/>
		<script>
			function validateform(){
				var x = document.forms[contact][Inquiry].value;
				if(x==Null||x==""){
					alert("This field is required");
					return false;
				}
				
				var y = document.forms[contact][F-name].value;
				if(y==Null||y==""){
					alert("This field is required");
					return false;
				}
				
				var z = document.forms[contact][L-name].value;
				if(z==Null||z==""){
					alert("This field is required");
					return false;
				}
				
				var w = document.forms[contact][email].value;
				if(w==Null||w==""){
					alert("This field is required");
					return false;
				}
			}
		</script>
	</head>
	
	<body>
		<div class="first">
			<div class="text">
				<h1>CONTACT US</h1>
				<p>We'd love to hear what you think about Magento.
				If you want to reach our sales team, have any questions, concerns,
				or just want to tell us how excited you are about Magento, drop us a note.</p>
				<p>Headquarters: 54 N. Central Ave, Suite 200, Campbell, CA 95008</p>
				<p>Please select the nature of your inquiry in the form below:</p>
			</div>
		</div>
		<div class="second">
			<div class="m-form">
				<p>We were unable to load the form. If you have an ad blocker or tracking protection running, try disabling it.</p>
				<form name="contact" action="" onsubmit="return validateform()" method="POST">
					<p>Reason For Inquiry:*</p>
					<input type="text" name="mail_inquery">
					<p>First Name:*</p>
					<input type="text" name="mail_f_name">
					<p>Last Name:*</p>
					<input type="text" name="mail_l_name">
					<p>Email Address:*</p>
					<input type="text" name="mail_email"><br><br>
					<div class="btn">
					<input type="submit" name="submit" value="SEND MESSAGE">
					</div>
				</form>
			</div>
		</div>
	</body>
</html>