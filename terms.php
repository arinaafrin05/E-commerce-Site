<html>
	<head>
		<title>Terms of service</title>
		<link rel="stylesheet" type="text/css" href="font_end/term.css"/>
	</head>
	<body>
		<div class="terms">
			<h2>TERMS OF SERVICE</h2>
			<h4>FOR USERS OF ANY SOPHIA OWNED WEBSITE</h4>
				<ul>
					<li><b>Privacy Policy</b></li>
					<li><b>Sophia Website Agreement</b></li>
				</ul>
			<h4>PRODUCT SPECIFIC AGREEMENTS</h4>
				<ul>
					<li><b>Sophia Connect Agreement</b></li>
					<li><b>Sophia U Terms and Conditions</b></li>
					<li><b>Sophia Enterprise Edition Subscription Agreement</b></li>
					<li><b>Sophia Expert Consulting Group Terms and Conditions</b></li>
					<li><b>Sophia Mobile End User License Agreement</b></li>
				</ul>
			<p>All visitors to a Sophia owned website are bound to the terms and conditions of Agreements 1 and 2. The other agreements apply to 
				users of those specific products.</p>
		</div>
	</body>
</html>